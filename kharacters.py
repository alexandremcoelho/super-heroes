import services

filename = "characters.csv"

found_character = services.find_all_characters(filename)
print(found_character)

character_id = 0

filename = "characters.csv"
character_id = 1


found_character = services.find_character_by_id(filename, character_id)
print(found_character)

new_character = {
    "name": "Hulk",
    "intelligence": 9,
    "power": 7,
    "strength": 10,
    "agility": 8
}
created_character = services.create_character(filename, **new_character)
print(created_character)