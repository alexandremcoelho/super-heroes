from .find_all_char import find_all_characters
from csv import DictWriter

def create_character(filename, **kwargs):
    filename = "characters.csv"

    found_characters = find_all_characters(filename)
    kwargs["id"] = len(found_characters) + 1
    found_characters.append(kwargs)
    with open(filename, "w") as filename:
        fieldnames = found_characters[0].keys()
        writer = DictWriter(filename, fieldnames=fieldnames)
        writer.writeheader()
        writer.writerows(found_characters)

    return kwargs

