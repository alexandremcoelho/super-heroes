from csv import DictReader
import os

def find_all_characters(file: str) -> list:
    if not os.path.exists(file) or os.stat(file).st_size == 0:
        return []
    with open(file, "r") as file:
        return [{'name': char['name'],
        'intelligence': int(char['intelligence']),
        'power': int(char['power']),
        'strength': int(char['strength']),
        'agility': int(char['agility']),
        'id': int(char['id'])} for char in DictReader(file)]
