from csv import DictReader
import os

def find_character_by_id(file, character_id):
    if not os.path.exists(file) or os.stat(file).st_size == 0:
        return []
    with open(file, "r") as file:
        output = [{'name': char['name'],
        'intelligence': int(char['intelligence']),
        'power': int(char['power']),
        'strength': int(char['strength']),
        'agility': int(char['agility']),
        'id': int(char['id'])} for char in DictReader(file) if int(char["id"]) == character_id]
        if(output):
            return output[0]
        return None